    # WC Schedule Planner - plan your next semester easily
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import MS.gui.serverapp
import Libraries.webserver.wsgiserver
import cherrypy
from cherrypy import wsgiserver
from cherrypy.process.servers import ServerAdapter


stuffs=Libraries.webserver.wsgiserver.QuizServer()
server = wsgiserver.CherryPyWSGIServer(('0.0.0.0', 9999), stuffs,server_name='10.0.0.99')
s1=ServerAdapter(cherrypy.engine,server)
s1.subscribe()
cherrypy.engine.start()
cherrypy.engine.block()

