import reaction
import bs4
import os
import random
class ReactionManager(object):
	def __init__(self,reactionsLocation):
		self.reactions=set()
		self.reactionIDs={}
		self.reactionSets={}
		for aFile in os.listdir(reactionsLocation):
			if '.xml' in aFile:
				xmlfile=open( os.path.join(reactionsLocation,aFile) ,'r')
				self._inputXML_(xmlfile.read())
				xmlfile.close()
		print self.reactions
		print self.reactionIDs
		print self.reactionSets
		
	def _inputXML_(self,XMLData):
		#print XMLData
		XMLP=bs4.BeautifulSoup(XMLData)
		#top level, look at sets in the file...
		for aSet in XMLP.find_all('set'):
			setTitle=aSet.settitle.contents[0]
			setRXNs=set()
			for aReaction in aSet.find_all('reaction'):
				name=aReaction.rxnname.contents[0]
				reactants=[]
				conditions=aReaction.conditions.contents[0]
				products=[]
				for reactant in aReaction.reactants.find_all('reactant'):
					reactants.append(str(*reactant.contents))
				for product in aReaction.products.find_all('product'):
					products.append(str(*product.contents))
				print name,reactants,conditions,products
				UUID=name+str(abs(hash(name+conditions+reactants[0]+products[0])))
				newRXN=reaction.Reaction(name,{'reactants':reactants,'conditions':conditions,'products':products},UUID)
				self.reactions.add(newRXN)
				setRXNs.add(newRXN)
				self.reactionIDs[UUID]=newRXN
			self.reactionSets[setTitle]=self.reactionSets.get(setTitle,set()) | setRXNs
			
	def getReactionsForSet(self,fromsets=[]):
		print fromsets
		chosenRXNs=set()
		if fromsets==[]:
			chosenRXNs=self.reactions
		for someset in fromsets:
			chosenRXNs=chosenRXNs.union(self.reactionSets[someset])
		return chosenRXNs
		
	def getRandomReaction(self,fromsets=[]):
		return random.choice( list(self.getReactionsForSet(fromsets)) )
		
	def getRandomReactionForUser(self,usermanager,username,fromsets=[]):
		for x in xrange(100):
			selectedRXN=random.choice( list(self.getReactionsForSet(fromsets)) )
			userResult=sorted([usermanager.getResult(username,selectedRXN.UUID),.05,.9])[1]
			if userResult<random.random():
				break
		return selectedRXN

		
testman = ReactionManager('reactionslol')
print testman.getRandomReaction().name