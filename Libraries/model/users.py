import os, sys
import cPickle as pickle

class aUser:
	def __init__(self,username,storageDirectory,password):
		print "writing new user", username
		self.username=username
		self.password=password
		self.storageDirectory=storageDirectory
		self.reactionResults={}
		self.reactionResultFractions={}
		self.writeSelf()

	def deleteSelf(self):
		userfileObject=os.remove(os.path.join(self.storageDirectory,self.username))
		print "deleted userdata for",self.username

	def writeSelf(self):
		userfileObject=open(os.path.join(self.storageDirectory,self.username),"w")
		loadedUser=pickle.dump(self,userfileObject)
		userfileObject.close()
		
	def initializeReactions(self):
		self.reactions=reactions
		
	def saveResult(self,GUID,result):
		self.reactionResults[GUID]=self.reactionResults.get(GUID,[])+[result]

		accumulator=0
		resultsForPercentage=self.reactionResults[GUID]
		for scorevalue in resultsForPercentage:
			accumulator+=int(scorevalue)
		
		self.reactionResultFractions[GUID]=float(accumulator)/len(resultsForPercentage)
		print self.reactionResults,self.reactionResultFractions
		self.writeSelf()
		
	def cleanup(self,storageDir):
		if storageDir != self.storageDirectory:
			print "storage directory moved for user",self.username
			self.storageDirectory=storageDir
		print "checking if user", self.username, "needs cleaning"
		self.writeSelf()




class userManager:
	def __init__(self,userDirectory,salt):
		self.userDirectory=userDirectory
		self.users=[]
		self.usernames={}
		userFiles=os.listdir(self.userDirectory)
		userFiles  = filter(lambda fname: not fname.startswith("."), userFiles)
		self.salt=salt

		for userfile in userFiles:
			userfileObject=open(os.path.join(userDirectory,userfile),"r")
			loadedUser=pickle.load(userfileObject)
			userfileObject.close()
			self.users.append(loadedUser)
			self.usernames[loadedUser.username]=loadedUser
			print "unpickled user", loadedUser.username,"|",
			loadedUser.cleanup(self.userDirectory)
		
	def newUser(self,username,password):
		newUser=aUser(username,self.userDirectory,self.hashPW(password))
		self.users.append(newUser)
		self.usernames[username]=newUser
		
	def removeUser(self,username):
		self.usernames[username].deleteSelf()
		self.users.remove(self.usernames[username])
		del(self.usernames[username])
		print username,"has been removed from the users list"
		
	def authenticate(self,username,password):
		if username in self.usernames.keys():
			return self.usernames[username].password == self.hashPW(password)
		return False
		
	def saveResult(self,username,rxnid,result):
		self.usernames[username].saveResult(rxnid,result)
		
	def getResult(self,username,rxnid):
		return self.usernames[username].reactionResultFractions.get(rxnid,0.0)
		
	def hashPW(self,pw):
		import hashlib
		return hashlib.sha512(pw + self.salt).hexdigest()