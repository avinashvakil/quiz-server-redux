Chemo={}
def convertChemical(informat,outformat,inputMolecule,gen2d=False,noName=False):
	while len(Chemo.keys())>100:
		print "Pruning the molecule cache..."
		del Chemo[random.choice(Chemo.keys())]
	if inputMolecule+informat+outformat not in Chemo.keys():
		print "don't know this molecule..."
		import openbabel
		obConversion = openbabel.OBConversion()
		obConversion.SetInAndOutFormats(informat,outformat)
		
		mol = openbabel.OBMol()	
		obConversion.ReadString(mol, inputMolecule)
		if gen2d:
			gen2d = openbabel.OBOp.FindType("gen2d") # Looks up the plugin and returns it
			gen2d.Do(mol) # Calls the plugin on your molecule
		if noName:
			obConversion.AddOption('n')

		
		Chemo[inputMolecule+informat+outformat]=obConversion.WriteString(mol)
		
		return Chemo[inputMolecule+informat+outformat]
	else:
		print "already know molecule..."
		return Chemo[inputMolecule+informat+outformat]