import chemconverter
class Reaction(object):
	def __init__(self,name,information,UUID):
		self.name       = name
		self.reactants  = information['reactants']
		self.products   = information['products']
		self.conditions = information['conditions']
		self.UUID=UUID
		
		self.answers   = []
		for product in self.products:
			self.answers.append(chemconverter.convertChemical('SMI','inchikey',product,gen2d=False))
		self.answers = set(self.answers)
		
	def checkAnswers(self,answersProvided,inFormat):
		usersINCHIKEYS=set()
		for answerProvided in answersProvided:
			usersINCHIKEYS.add(chemconverter.convertChemical(inFormat,'inchikey',answerProvided,gen2d=False))
		print usersINCHIKEYS, self.answers
		if usersINCHIKEYS==self.answers:
			return "CORRECT"
		elif usersINCHIKEYS<self.answers:
			return "WRONG NOT ALL ANSWERS"
		elif usersINCHIKEYS>self.answers:
			return "WRONG TOO MANY ANSWERS"
		else:
			return "WRONG"
			
	def renderMOL(self,eliminateN=False):
		newDict={}		
		newDict['reactants']=map(lambda x:chemconverter.convertChemical('SMI','SDF',x,gen2d=True),self.reactants)
		newDict['products']=map(lambda x:chemconverter.convertChemical('SMI','SDF',x,gen2d=True),self.products)
		
		if eliminateN:
			newDict['reactants']=map(lambda x:x.replace("\n","\\n"),newDict['reactants'])
			newDict['products']=map(lambda x:x.replace("\n","\\n"),newDict['products'])

		return newDict
	
	def __repr__(self):
		return "RXN-NAMED:"+self.name
			
			
testRXN = Reaction('test',{'reactants':['Cc1ccccc1'],'products':['Cc1ccc(cc1[N+](=O)[O-])[N+](=O)[O-]'],'conditions':'None'},'lolxd')
print testRXN.checkAnswers(['Cc1ccc(cc1[N+](=O)[O-])[N+](=O)[O-]'],'smi')
print testRXN.checkAnswers(['Molecule Name\n  CHEMDOOD03201316583D 0   0.00000     0.00000     0\n[Insert Comment Here]\n 13 13  0  0  0  0  0  0  0  0  2 V2000\n    0.8660    1.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n    1.7321    0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n    0.0000    0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n    0.8660    2.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n    1.7321   -0.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n    0.0000   -0.2500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n   -0.8660    1.2500    0.0000 N   0  3  0  0  0  0  0  0  0  0  0  0\n    0.8660   -0.7500    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\n   -1.7321    0.7500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n   -0.8660    2.2500    0.0000 O   0  5  0  0  0  0  0  0  0  0  0  0\n    0.8660   -1.7500    0.0000 N   0  3  0  0  0  0  0  0  0  0  0  0\n    0.0000   -2.2500    0.0000 O   0  5  0  0  0  0  0  0  0  0  0  0\n    1.7321   -2.2500    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\n  1  2  1  0  0  0  0\n  3  1  2  0  0  0  0\n  1  4  1  0  0  0  0\n  2  5  2  0  0  0  0\n  6  3  1  0  0  0  0\n  3  7  1  0  0  0  0\n  5  8  1  0  0  0  0\n  8  6  2  0  0  0  0\n  7  9  2  0  0  0  0\n  7 10  1  0  0  0  0\n  8 11  1  0  0  0  0\n 11 12  1  0  0  0  0\n 11 13  2  0  0  0  0\nM  CHG  4   7   1  10  -1  11   1  12  -1\nM  END'],'SDF')