from bottle import *
import datetime
from ..model import reactionManager as RM
from ..model import users as US

class QuizServer(Bottle):
		def __init__(self):
				super(QuizServer, self).__init__()
				self.initializeRoutes()
				self.initDataStructs()
				self.secretAdmin='lolol'
				
		def initDataStructs(self):
				self.rxman=RM.ReactionManager('reactionslol')
				self.userman=US.userManager('testusers','testsalt')
				self.setkeys={}
				n=0
				for aset in self.rxman.reactionSets.keys():
					self.setkeys[str(n)]=aset
					n+=1

		def initializeRoutes(self):
			
				def requiresAuthentication(original_function):
					# make a new function that prints a message when original_function starts and finishes
					def new_function(*args, **kwargs):
						loginSessionTimeout=600
						print "Authenticated Page Requested", request.path
						if not request.get_cookie("account", secret=self.secretAdmin) or not request.get_cookie("settime", secret=self.secretAdmin):
							redirect("/")
						cookieSetTime=request.get_cookie("settime", secret=self.secretAdmin)
						cookieTimeDelta=datetime.datetime.now()-cookieSetTime
						if loginSessionTimeout<cookieTimeDelta.total_seconds():
							redirect("/")
						print "Session Active For:",cookieTimeDelta
						response.set_cookie("settime",datetime.datetime.now(),path="/", secret=self.secretAdmin)
						return original_function(*args, **kwargs)
					return new_function

				@self.route('/static/<filename:path>')
				def server_static(filename):
						return static_file(filename, root='Static')
												
				@self.route('/<selectedsets>/reaction')
				@requiresAuthentication
				def randomRXN(selectedsets):
					#draw template for rendering
					from bottle import SimpleTemplate
					temp=open('views/quizQuestionView copy.html','r+')
					templatefile=temp.read()
					temp.close()
					newtemplate=SimpleTemplate(templatefile)
					
					selectedsetnames=[]
					selectedsets=selectedsets.split('+')
					for setID in selectedsets:
						selectedsetnames.append(self.setkeys[setID])
						
					username=request.get_cookie("account", secret=self.secretAdmin)
					
					return newtemplate.render(rxn=self.rxman.getRandomReactionForUser(self.userman,username,selectedsetnames))
					
				@self.route('/menu')
				@requiresAuthentication
				def mainMenu():
					#draw template for rendering
					from bottle import SimpleTemplate
					temp=open('views/mainmenu.html','r+')
					templatefile=temp.read()
					temp.close()
					newtemplate=SimpleTemplate(templatefile)
					
					username=request.get_cookie("account", secret=self.secretAdmin)
					
					return newtemplate.render(setList=self.setkeys)

				@self.route('/makereaction')
				@requiresAuthentication
				def makeRXN():
					#draw template for rendering
					from bottle import SimpleTemplate
					temp=open('views/reactionMaker.html','r+')
					templatefile=temp.read()
					temp.close()
					newtemplate=SimpleTemplate(templatefile)
					
					username=request.get_cookie("account", secret=self.secretAdmin)
					
					return newtemplate.render(setList=self.setkeys)

				@self.route('/redirectToReaction',method="POST")
				def redirectRXN():
					reqstr=""
					for rxnset in self.setkeys.keys():
						if request.forms.get('set'+str(rxnset)):
							reqstr+=str(rxnset)+'+'
							
					reqstr=reqstr[:-1]
					print reqstr
					return redirect(reqstr+'/reaction')


				@self.route('/checkAnswer',method="POST")
				def randomRXN():
					answer=str(request.forms.answer)
					answer=answer.split("$$$")
					result= self.rxman.reactionIDs[answer[0]].checkAnswers(answer[1:],"SDF")
					
					return result

				@self.route('/generateXML',method="POST")
				def genXML():
					rxndata={}
					rxndata['reactant']= request.forms.reactant.split('$$$')[1:]
					rxndata['product']= request.forms.product.split('$$$')[1:]
					rxndata['name']=request.forms.name
					rxndata['set']=request.forms.set
					rxndata['condition']=request.forms.conditions
					
					print rxndata
					
					from ..model import chemconverter
					molrewrite=lambda x:chemconverter.convertChemical('SDF','SMI',str(x),gen2d=False,noName=True).rstrip()
					print rxndata['reactant']
					rxndata['reactant']=map(molrewrite,rxndata['reactant'])
					rxndata['product']=map(molrewrite,rxndata['product'])
					
					temp=open('views/newRXN.xml','r+')
					templatefile=temp.read()
					temp.close()
					newtemplate=SimpleTemplate(templatefile)

					return newtemplate.render(rxndata=rxndata)

					return result


				@self.route('/login',method='POST')
				def loginUser():
					username=str(request.forms.username)
					password=str(request.forms.password)
					print username,password
					if self.userman.authenticate(username,password):
						print username, "logged in"
						response.set_cookie("account", username,path="/", secret=self.secretAdmin)
						response.set_cookie("settime",datetime.datetime.now(),path="/", secret=self.secretAdmin)
						return redirect("/menu")
					else:
						print "invalid user"
						
				@self.route('/logout',method='GET')
				def logout():
					response.delete_cookie("account")
					response.delete_cookie("settime")
					return redirect("/")
					
				@self.route('/')
				def sendToTiles():
					return template("loginWindow.html")

				@self.route('/newUser/<username>/<password>/<adminPass>')
				def newUser(username,password,adminPass):
					if adminPass==self.secretAdmin:
						try:
							self.userman.newUser(username,password)
							return "user created with name ",username
						except:
							return "failed to create user"
					else:
						print "wrong password"
